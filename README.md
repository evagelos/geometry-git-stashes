# Git stashes
[Geometry's](https://github.com/geometry-zsh/geometry) prompt plugin to show if "git stashes" exist

![](screenshot.png)

## Installation

```sh
zplug "ev-agelos/geometry-git-stashes"
```

## Configuration

```sh
GEOMETRY_COLOR_GIT_STASHES=yellow
GEOMETRY_SYMBOL_GIT_STASHES=📌
```
